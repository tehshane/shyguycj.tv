import { nodeResolve } from '@rollup/plugin-node-resolve';
import { terser } from "rollup-plugin-terser";
import replace from '@rollup/plugin-replace';

const isProduction = process.env.NODE_ENV === 'production';

export default [
  {
    plugins: [
      nodeResolve()
    ],
    input: 'public/service-worker.js',
    output: {
      file: 'public/service-worker.min.js',
      format: 'umd',
      plugins: [
        terser(),
        replace({
          'process.env.NODE_ENV': JSON.stringify(isProduction ? 'production' : 'development'),
        })
      ]
    },
    treeshake: 'recommended'
  }
]
