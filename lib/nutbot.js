/**
 * Watch CJ's channel for nut emotes
 */

require('dotenv').config();
const debug = require('debug')('cj:nutbot');
const EventEmitter = require('events');
const tmi = require('tmi.js');

let tmiClient;
const emitter = new EventEmitter();

module.exports = {
  emitter,
  goOnline,
  goOffline,
};

_init();

/////

async function _init () {

  tmiClient = new tmi.Client({
    options: {
      debug: false,
      messagesLogLevel: 'warn'
    },
    connection: {
      reconnect: true,
      secure: true
    },
    identity: {
      username: process.env.TMI_MODERATOR_USERNAME,
      password: process.env.TMI_MODERATOR_PASSWORD
    },
    channels: [
      'shyguycj'
    ]
  });

  tmiClient.on('message', onMessage);

}

/**
 * Go online when the streamer goes online
 */
async function goOnline () {
  debug((new Date()).toLocaleString(), `NutBot going online`);
  await tmiClient.connect();
}

/**
 * Disconnect when the streamer goes offline
 */
async function goOffline () {
  debug((new Date()).toLocaleString(), `NutBot going offline`);
  await tmiClient.disconnect();
}

/**
 * Fires whenever a message is received in chat
 * @param {string} channel The channel the message was sent to
 * @param {object} userstate Details about the user who sent the message
 * @param {string} message The message body
 * @param {boolean} self Whether this is me
 * @returns null
 */
async function onMessage (channel, userstate, message, self) {

  const nutMatch = message.match(/(shyguy24[\w]*nut[\w]*)|(nutJAM)/ig);
  if (nutMatch && nutMatch.length > 0) {
    debug(`Incrementing ${nutMatch.length} nuts`);
    emitter.emit('nuts', nutMatch.length);
  }

}
