process.env.TZ = 'America/Los Angeles';
const PORT = process.env.PORT || 3000;

require('dotenv').config();
const debug = require('debug')('cj');
const twitchDebug = debug.extend('twitch');
const Sentry = require("@sentry/node");
const Tracing = require("@sentry/tracing");
const { differenceInHours, isAfter } = require('date-fns');


debug(new Date().toLocaleString());

Sentry.init({
  dsn: process.env.SENTRY_DSN,

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});

const { join } = require('path');
const { twitch } = require('@tehshane/shanebot');
const { checkUserSub, getUserCreatedDate, passportCallback, emitter: twitchEmitter } = twitch;
const fastify = require('fastify')();
const fastifyPassport = require('fastify-passport');
const fastifySecureSession = require('fastify-secure-session');
const Redis = require('redis');
const TwitchStrategy = require('passport-twitch-new').Strategy;

const redis = Redis.createClient({
  host: process.env.REDIS_HOST || 'localhost'
});

const callbackHost = process.env.NODE_ENV === 'production'
  ? 'https://shyguycj.tv'
  : 'http://localhost:3000'

fastify.register(fastifySecureSession, { key: process.env.SESSION_SECRET })
fastify.register(fastifyPassport.initialize());
fastify.register(fastifyPassport.secureSession());

const twitchStrategyOptions = {
  clientID: process.env.TWITCH_CLIENT_ID,
  clientSecret: process.env.TWITCH_CLIENT_SECRET,
  callbackURL: `${callbackHost}/auth/twitch/callback`,
  scope: [
    'channel:read:redemptions',
    'channel:read:subscriptions',
  ].join(' '),
};

fastifyPassport.use('twitch', new TwitchStrategy(twitchStrategyOptions, passportCallback));

fastify.register(require('fastify-websocket'), {
  clientTracking: true
});

/**
 * For the month of September, subs
 */

// Static file handler
fastify.register(require('fastify-static'), {
  root: join(__dirname, 'public')
});

// Form handler
fastify.register(require('fastify-formbody'));

const siteMetadata = {
  title: 'ShyguyCJ',
  description: 'Music major/voice teacher gone streamer',
  avatar: 'https://static-cdn.jtvnw.net/jtv_user_pictures/07e7de12-c35c-48bd-b568-ed3cc3b09bef-profile_image-300x300.png',
  siteName: 'ShyguyCJ on Twitch',
  path: '/',
};

function title (titleText) {
  if (!titleText) return siteMetadata.siteName;
  return `${titleText} | ${siteMetadata.title}`;
}

// View renderer
fastify.register(require('point-of-view'), {
  engine: {
    ejs: require('ejs')
  }
});

fastify.get('/', (req, res) => {
  res.view('views/index.ejs', { ...siteMetadata, title: title() });
});

fastify.get('/nut', (req, res) => {
  res.view('views/dvd.ejs', { ...siteMetadata, title: title('N U T'), path: '/nut' });
});

fastify.get('/count', (req, res) => {
  res.view('views/click.ejs', { ...siteMetadata, title: title('Clicky'), path: '/count' });
});

fastify.get('/thanks', (req, res) => {
  const expired = isAfter(new Date(), new Date(2021,9,1,7,0,0)) || req.query.e; // October 1, 2021 00:00 PDT
  res.view('views/thanks.ejs', { ...siteMetadata, title: title('Thank you!'), path: '/thanks', notSubbed: false, expired });
});

fastify.post('/thanks', async (req, res) => {

  const transaction = Sentry.startTransaction({
    op: 'thanks-lookup',
    name: 'Thanks Card lookup'
  });

  // Validate the username
  const username = (req.body.username || '').toLowerCase().trim().replace(/[^\w\d\_\-\.]+/g, '').substr(0, 24);
  if (!username) {
    return res.view('views/thanks.ejs', { ...siteMetadata, title: title('Thank you!'), path: '/thanks', notSubbed: true });
  }

  // Verify the username against Twitch API
  const sub = await checkUserSub(username);

  if (sub) {

    // Redirect to Google Form
    res.redirect(302, `https://docs.google.com/forms/d/e/1FAIpQLSfyOHGcXQ4oWN39slShlcg4iA7gU-ZK806k3ec5gwuNotUP2Q/viewform?usp=pp_url&entry.1847857318=${sub}`);

  } else {

    // Return to this page with an error
    res.view('views/thanks.ejs', { ...siteMetadata, title: title('Thank you!'), path: '/thanks', notSubbed: true });

  }

  transaction.finish();

});

// Nightbot Commands
fastify.get('/nightbot/yo', (req, res) => {

  /**
   * Old !yo command
   * $(eval let o="$(1)"!=="null"?"$(1)":"$(touser)";if(o.toLowerCase().includes("nightbot"))o="me";if(o.toLowerCase().includes("shyguycj"))o="daddy CJ";const t=[`Yo ${o}, what's up?! How's your night going?`,`Yo, what's up ${o}? How's your night going?`,`Yo, ${o}! Welcome!`,`Yo, ${o}! Welcome to the first day of the rest of your life!`,`Yo, ${o}, what's up dude?`];t[Math.floor(Math.random()*t.length)];)
   */

  if (req.query && req.query.q) {

    let username = req.query.q;

    // Special usernames
    if (!username.startsWith('@')) username = `@${username}`;
    if (username.includes('nightbot')) username = 'me';
    if (username.includes('shyguycj')) username = 'daddy CJ';

    const yoPhrases = [
      `Yo ${username}, what's up?! How's your night going?`,
      `Yo, what's up ${username}? How's your night going?`,
      `Yo, ${username}! Welcome!`,
      `Yo, ${username}! Welcome to the first day of the rest of your life!`,
      `Yo, ${username}, what's up dude?`
    ];

    return res
      .status(200)
      .send(yoPhrases[Math.floor(Math.random() * yoPhrases.length)]);

  } else {

    redis.get('yo', (err, result) => {
      if (err) return res.status(200).send('Yo!');
      result++;
      res.status(200).send(`Yo! count: ${result}`);
      redis.set('yo', result);
    });

  }

});

fastify.get('/nightbot/handbook/:type', (req, res) => {

  const rules = {
    killer: [
      'After hooking a survivor, move to the opposite end of the map and stare at the corner for five minutes or else you\'re a tunneler.',
      'Setting traps as Trapper is game hacking and is a reportable offense.',
      'Your job is to make the game entertaining for survivors. Hooking them is not fun and must be avoided.',
      'Hooking survivors is toxic. Don\'t do it.',
      'Hooking the same survivor more than once per match is tunnelling.',
      'Playing Spirit is against DBD\'s TOS and will be reported.',
      'Kicking pallets is vandalism.',
      'Kicking or hitting gens is toxic. You wouldn\'t kick a child\'s sand castle, so don\'t kick a gen! The survivors worked hard on it!',
      'All survivors must be allowed to escape or else you\'re holding the game hostage.',
      'Not allowing survivors to heal is toxic.',
      'Huntress\' Iridescent Head add-on is considered a game-breaking bug and must not be used.',
    ],
    survivor: [
      'The killer worked hard to chase and injure you. Don\'t insult their efforts by healing or using Self Care.',
      'Flashlight saving other surivors is interfering with the game.',
      'The Camaraderie perk is classified as "holding the game hostage" and is toxic.',
      'Haddonfield tokens are a mortal sin and must never be used.',
      'Raccoon City Police Station / RPD tokens are a mortal sin and must never be used.',
      'The use of flashlights is considered trolling and is prohibited.',
      'Using Lithe is toxic.',
      'Decisive Strike is widely considered to be a toxic perk. The killer picked you up for a reason and you should respect that.',
    ]
  };

  const index = Math.floor(Math.random() * rules[req.params.type].length);

  return res
    .status(200)
    .send(`${capitalizeFirstLetter(req.params.type)} Handbook, Rule #${index + 1}: ${rules[req.params.type][index]}`);

});

fastify.get('/nightbot/yo/reset', (req, res) => {

  redis.set('yo', 0, (err, result) => {
    if (err) return res.status(200).send('Uh oh, yo.');
    return res.status(200).send('No mo yo.');
  });

});

fastify.get('/nightbot/nut', (req, res) => {

  redis.INCR('nut', (err, nuts) => {
    if (err) return res.status(200).send('Yo!');
    res.status(200).send(`Nut Nation has nutted ${nuts.toLocaleString()} ${nuts === 1 ? 'time' : 'times'}!`);
    broadcast(nuts);
  });

});

fastify.get('/nightbot/nut/reset', (req, res) => {

  redis.set('nut', 0, (err, result) => {
    if (err) return res.status(200).send('Can\'t stop the nut!');
    broadcast(0);
    return res.status(200).send('No nut November? No nut NOW.');
  });

});

fastify.get('/nutsocket', { websocket: true }, (connection /* SocketStream */, req /* FastifyRequest */) => {

  // Send nut count on connection
  redis.get('nut', (err, nuts) => {
    if (err) return false;
    connection.socket.send(nuts);
  });

  // Increment the nut counter
  connection.socket.on('message', message => {
    redis.INCR('nut', (err, nuts) => {
      if (!err) broadcast(nuts);
    });
  });

  connection.socket.on('close', () => {
    // noop
  });

});

if (process.env.AUTH_ENDPOINT_ENABLED === 'true') {

  console.warn('Auth endpoints are enabled');

  fastify.get('/auth/twitch',
    { preValidation: fastifyPassport.authenticate('twitch', { forceVerify: true, authInfo: false }) },
    async () => ''
  );

  fastify.get('/auth/twitch/callback',
    { preValidation: fastifyPassport.authenticate('twitch', { successRedirect: '/thanks', authInfo: false }) },
    async () => ''
  );

}

function broadcast (nuts) {
  fastify.websocketServer.clients.forEach(client => {
    if (client.readyState === 1) {
      client.send(nuts);
    }
  })
}

fastify.listen(process.env.PORT || 3000, (err) => {
  if (err) return console.error(err);
  console.log(`Listening at http://localhost:${process.env.PORT || 3000}`);
});

try {

  const nutbot = require('./lib/nutbot');

  nutbot.emitter.on('nuts', (nuts) => {
    if (nuts > 0) redis.INCRBY('nut', nuts, (err, newNuts) => {
      if (err) return console.warn(`Failed to nut: ${err.message}`);
      broadcast(newNuts);
    });
  });

  twitchEmitter.on('cheer', cheer => {
    if (cheer.bits === 420) {
      redis.INCRBY('nut', cheer.bits, (err, newNuts) => {
        if (err) return console.warn(`Failed to nut: ${err.message}`);
        broadcast(newNuts);
      });
    }
  });

  twitchEmitter.on('redeem', redemption => {
    if (redemption.rewardTitle.toLowerCase() === 'nut') {
      redis.INCR('nut', (err, newNuts) => {
        if (err) return console.warn(`Failed to nut: ${err.message}`);
        broadcast(newNuts);
      });
    }
  });

  twitchEmitter.on('offline', () => {
    nutbot.goOffline();
    console.info('-- Offline --');
  });

  twitchEmitter.on('online', () => {
    nutbot.goOnline();
    console.info('-- Online --')
  });

  twitchEmitter.on('follow', async (channel, username) => {
    const createDate = await getUserCreatedDate(username);
    if (createDate) {
      const hourDiff = differenceInHours(new Date(), createDate);
      if (hourDiff <= 12) {
        console.warn(`!!! ${username} was created ${hourDiff} hours ago!`);
      }
    }
  });

} catch (error) {
  console.warn('Nutbot failed to load', error.message);
}

try {
  const { shanebot } = require('@tehshane/shanebot');

  twitchEmitter.on('cheer', cheer => twitchDebug(`${cheer.userDisplayName} cheered ${cheer.bits} bits`));
  twitchEmitter.on('follow', shanebot.onFollow);
  twitchEmitter.on('offline', shanebot.goOffline);
  twitchEmitter.on('online', shanebot.goOnline);
  twitchEmitter.on('redeem', redemption => twitchDebug(`${redemption.userDisplayName} redeemed ${redemption.rewardTitle} ${redemption.input ? `(${redemption.input})` : ''}`));

} catch (error) {
  console.error('ShaneBot failed to load', error.message);
}

/////

function capitalizeFirstLetter (string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
